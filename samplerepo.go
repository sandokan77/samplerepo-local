package samplerepo

import (
	"log"
)

// Func1 is a simple function. It's purpose is to test if this package can be accessed from the outside world
func Func1() {

	log.Println("*** Func1 calledx")
}

// Func2 is a simple function. It's purpose is to test if this package can be accessed from the outside world
func Func2() {

	log.Println("*** Func2 called")
}
